from django import forms

GENDER = (
(None, 'Choose your gender'),
('male', 'male'),
('female', 'female'),
('custom', 'custom'),
('Prefer Not To Say', 'Prefer Not To Say'),
)
class GenderField(forms.ChoiceField):
 def __init__(self, *args, **kwargs):
  super(GenderField, self).__init__(*args, **kwargs)
  self.choices = ((None,'Select gender'),('M','Male'),('F','Female'))
class DateInput(forms.DateInput):
 input_type='date'
class LoginForm(forms.Form):
 name = forms.CharField()
 gender = forms.ChoiceField(choices=GENDER)
 dateofbirth= forms.DateField(widget = DateInput)
 city= forms.CharField()
 state= forms.CharField()
 country= forms.CharField()
