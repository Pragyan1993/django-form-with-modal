from django.db import models

class Post(models.Model):
 name = models.CharField(max_length=100)
 gender = models.CharField(max_length=10)
 date_of_birth = models.DateField()
 city = models.CharField(max_length=50)
 state = models.CharField(max_length=50)
 country = models.CharField(max_length=50)
 def __str__(self):
     return self.name
