from django.shortcuts import render
from .forms import LoginForm
from .models import Post
def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)

        if form.is_valid():
            if request.POST.get("submit"):
                post=Post()
                name = request.POST.get('name', '')
                gender = request.POST.get('gender', '')
                dateofbirth = request.POST.get('dateofbirth', '')
                city = request.POST.get('city', '')
                state = request.POST.get('state', '')
                country = request.POST.get('country', '')
                return render(request, 'account/login.html', {'form': form,'successful_submit': True,'name':name,'gender':gender,'dateofbirth':dateofbirth,'city':city,'state':state,'country':country})
            elif request.POST.get("cancel"):
                form=LoginForm()
                return render(request, 'account/login.html', {'form': form})

    else:
        form=LoginForm()
    return render(request, 'account/login.html', {'form': form})
